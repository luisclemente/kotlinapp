package com.example.mykotlinapp

class TestClass {

    val a: Int = 1  // immediate assignment
    val b = 2   // `Int` type is inferred
    val c: Int = 3



}