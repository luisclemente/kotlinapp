plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    id ("maven-publish")
    id("org.jetbrains.dokka") version "1.9.10"
    id("nebula.javadoc-jar") version "18.4.0"
    id("nebula.source-jar") version "18.4.0"
}

android {
    namespace = "com.example.mylibrary"
    compileSdk = 34

    defaultConfig {
        minSdk = 22

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {

    implementation("androidx.core:core-ktx:1.12.0")
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("com.google.android.material:material:1.11.0")
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
}

/*_______NEXUS_______*/
publishing {
    publications {
        register<MavenPublication>("release") {
            groupId = "org.gradle.sample"
            artifactId = "library"
            version = "1.17"

            afterEvaluate {
                from(components["release"])
            }

            artifact("$buildDir/dokka/javadoc")
        }
    }

    repositories {
        maven {
            credentials {
                username = "${properties["libraryUser"]}"
                password = "${properties["libraryPassword"]}"
            }
            url = uri("http://localhost:8081/repository/maven-releases/")
            isAllowInsecureProtocol = true
        }
    }
}