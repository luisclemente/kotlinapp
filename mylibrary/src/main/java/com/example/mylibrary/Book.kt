package com.example.mylibrary

/**
 * A book of *articles*.
 *
 * This class is just a **documentation example**.
 *
 * @param T the type of article in this book.
 * @property name the name of this book.
 * @constructor creates an empty book.
 * @see com.baeldung.kdoc.TitledArticle
 * @sample com.baeldung.kdoc.blogSample
 */
class Book<T>(private val name: String)  {
    private var articles: MutableList<T> = mutableListOf()

    /**
     * Adds an [article] to this book.
     * @return the new number of articles of the book.
     */
    fun add(article: T): Int {
        articles.add(article)
        return articles.size
    }
}

/**
 * An article with a title.
 *
 * @property title the title of this article.
 * @constructor creates an article with a title.
 */
data class TitledArticle(private val title: String)

private fun bookSample() {
    val book = Book<TitledArticle>("Baeldung on Kotlin")
    val bookCount = book.add(TitledArticle("An Introduction to KDoc"))
}