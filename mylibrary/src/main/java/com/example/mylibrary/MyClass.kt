package com.example.mylibrary

class MyClass {
    fun greet(name: String) {
        println("Hi, $name")
    }
}